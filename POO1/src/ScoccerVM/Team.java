package ScoccerVM;

public class Team {
    public SoccerPlayer [] soccerPlayers = new SoccerPlayer[5];
    public String name;
    private Team enemy;

    public Team () {
        setTeam();
        name =  Utilities.GenerateTeamName();
    }

    public String getName (){
        return name;
    }

    public void setEnemy (Team enemy) {
        this.enemy = enemy;
    }

    public Team GetEnemy (){
        return enemy;
    }

    void setTeam (){
        for (byte i = 0; i< soccerPlayers.length; i++){
            soccerPlayers[i] = new SoccerPlayer();
            soccerPlayers[i].position = Utilities.GetPosition(i);
        }
    }
}
