package ScoccerVM;

public class Person {
    protected String name, lastname;


    public Person (){
        name = Utilities.GenerateName();
        lastname = Utilities.GenerateLastName();
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setName(String name) {
        this.name = name;
    }
}
