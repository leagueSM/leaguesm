package ScoccerVM;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class League {

    static Team[] teams = new  Team[6];
    static List<Integer> sortHelper =new ArrayList(Arrays.asList(3, 4, 5));

    public static void main  (String[] args){
        MakeTeams();
        SortTeams();
        PrintLeague();
        GetLeagueInfo();

    }

    static void GetLeagueInfo (){
        int option =0;
        do {
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++1");
            System.out.println("Get information from the team. Chose an option:");
            for (int i = 0; i < 6; i++) {

                System.out.println((1+i)+ ". " + teams[i].getName());
            }
            System.out.println("0. Exit program");

            Scanner input = new Scanner(System.in);
            option = input.nextInt();

         if(option>0)
             PrintTeamInfo(teams[option-1]);
        }while(option > 0);
    }

    static void PrintTeamInfo (Team team){
        for (int i = 0; i < team.soccerPlayers.length; i++) {

            System.out.println((1+i)+ ". " + team.soccerPlayers[i].getName() + " " + team.soccerPlayers[i].getLastname() + " " + team.soccerPlayers[i].getPosition());
        }
    }

    static void PrintLeague (){
        System.out.println("League");
        for (int i = 0; i < 3; i++) {

            System.out.println((1+i)+ ". " + teams[i].getName() +  " VS " + teams[i].GetEnemy().getName());
        }

    }

    static void SortTeams (){
        for (int i = 0; i<3;i++)
            teams[i].setEnemy(GetEnemy());
    }

    static  void MakeTeams (){
        for (int i = 0; i < teams.length; i++) {
            teams[i] = new Team();
        }
    }

    static Team GetEnemy (){
        int index = (int)(Math.random()* sortHelper.size());
        int tmp = sortHelper.get(index);
        sortHelper.remove(index);
        return   teams[tmp];

    }
}
