package ScoccerVM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utilities {
    static String[] names = {"Juan", "Camilo", "Andres", "Felipe", "Gustavo", "Sebastian", "Alirio", "Jhonatna", "Diego", "Fernado"};
    static String[] lastNames = {"Murcia", "Rodriguez", "Calamaro", "Uzumaki", "Uchiha", "Mora", "Arias", "Mazo", "Robayo", "Cortez"};
    static String[] positions = {"Goalkeeper", "Defender", "Defender","Midfielder ", "Forward "};
    static List<String> teamNames = new ArrayList(Arrays.asList("BlackEagles", "BlueKillers", "RedHackers", "LittleGirls", "BadBoy", "SuggarDaddies"));

    public static void SetProperties (){

    }

    public static String GenerateTeamName () {
        int index = (int)(Math.random()* teamNames.size()-1);
        String tmp = teamNames.get(index);
        teamNames.remove(index);
        return  tmp;
    }

    public static String GenerateName () {
        return names [(int)(Math.random()* names.length-1)];
    }
    public static String GenerateLastName () {
        return lastNames [ (int)(Math.random()* lastNames.length-1)];
    }

    public static  String GetPosition (byte index){
        return  positions[index];
    }

    public static byte  GenerateNumber (){
        return  (byte)(Math.random()* 10+1);
    }
}
